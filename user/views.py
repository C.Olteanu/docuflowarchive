from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from user.forms import UserExtendForm
from user.models import UserExtend


class UserCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'user/create_user.html'
    form_class = UserExtendForm
    success_url = reverse_lazy('list-users')
    permission_required = 'user.add_user'


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'user/list_users.html'
    model = UserExtend
    context_object_name = 'all_users'
    permission_required = 'user.view_user'

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['user_form'] = UserExtendForm(self.request.GET or None)
        return context


class UserUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'user/update_user.html'
    model = UserExtend
    fields = ['username', 'first_name', 'last_name', 'email', 'phone', 'department', 'title']
    success_url = reverse_lazy('list-users')
    permission_required = 'user.add_user'


class UserDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'user/delete_user.html'
    model = UserExtend
    context_object_name = 'user'
    success_url = reverse_lazy('list-users')
    permission_required = 'user.add_user'
