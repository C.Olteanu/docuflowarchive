from django.contrib.auth.models import User
from django.db import models


class UserExtend(User):
    phone = models.CharField(max_length=20)
    title = models.CharField(max_length=50)
    department = models.CharField(max_length=50)

