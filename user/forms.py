from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import TextInput, ModelForm

from user.models import UserExtend


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['username', 'first_name', 'last_name', 'email', 'department', 'title']
        widgets = {
            'username': TextInput(attrs={
                'placeholder': 'Introduceti numele de utilizator',
                'class': 'form-control form-control-sm'
            }),
            'first_name': TextInput(attrs={
                'placeholder': 'Introduceti numele utilizatorului',
                'class': 'form-control form-control-sm'
            }),
            'last_name': TextInput(attrs={
                'placeholder': 'Introduceti numele de familie al',
                'class': 'form-control form-control-sm'
            }),
            'email': TextInput(attrs={
                'placeholder': 'Introduceti adresa de email a utilizatorului',
                'class': 'form-control form-control-sm'
            }),
            'department': TextInput(attrs={
                'placeholder': 'Introduceti departamentul din care face parte',
                'class': 'form-control form-control-sm'
            }),
            'title': TextInput(attrs={
                'placeholder': 'Introduceti functia',
                'class': 'form-control form-control-sm'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['password2'].widget.attrs['class'] = 'form-control form-control-sm'
