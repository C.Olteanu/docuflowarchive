
from django.urls import path

from user import views

urlpatterns = [
    path('users/create-user/', views.UserCreateView.as_view(), name='create-user'),
    path('users/list-users/', views.UserListView.as_view(), name='list-users'),
    path('users/delete/<int:pk>/', views.UserDeleteView.as_view(), name='delete-users'),
    path('users/update/<int:pk>/', views.UserUpdateView.as_view(), name='update-user'),
]
