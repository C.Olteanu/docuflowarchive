# Generated by Django 3.2.5 on 2021-09-09 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0013_auto_20210909_1333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadedfile',
            name='expiration_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
