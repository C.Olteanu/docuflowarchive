from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML
from django import forms
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.shortcuts import render, redirect

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from archive.forms import ArchiveForm, CollectionForm, DocumentTemplateForm, RegionOfInterestForm, UploadFileForm
from archive.models import *
from user.models import UserExtend
from django.core.paginator import Paginator


class ArchiveCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'archive/create_archive.html'
    form_class = ArchiveForm
    success_url = reverse_lazy('list-archives')
    permission_required = 'archive.add_archive'

    def form_valid(self, form):
        form.instance.created_by = UserExtend.objects.get(id=self.request.user.id)
        return super().form_valid(form)


@login_required
def archive_create_view(request):
    if request.method == 'POST':
        post_dict = dict(request.POST.copy())
        print(request.POST)
        print(request.FILES)
        post_dict['created_by'] = UserExtend.objects.get(id=request.user.id)
        archive_form = ArchiveForm(data=request.POST)
        archive = archive_form.save()
        archive.created_by = UserExtend.objects.get(id=request.user.id)
        archive.save()
        collection = CollectionForm(data={
            'collection_name': post_dict['collection_name'][0],
            'archive': archive.id
        })
        collection.save()

        template_form = DocumentTemplateForm(request.POST, request.FILES)
        template = template_form.save()
        template.archive = archive

        template.save()
        region_form = RegionOfInterestForm(data=request.POST)
        region = region_form.save()
        region.document_template = template
        roi_list = post_dict['ocr']
        new_list = []
        for li in roi_list:
            new_list.append(ast.literal_eval(li))
        region.region_points = new_list
        region.save()
        return redirect('/archive/archive/list-archives/')
    archive_create_form = ArchiveForm()
    collection_form = CollectionForm()
    document_template_form = DocumentTemplateForm()
    roi_form = RegionOfInterestForm()
    context = {
        'archive_create_form': archive_create_form,
        'collection_form': collection_form,
        'document_template_form': document_template_form,
        'roi_form': roi_form

    }
    return render(request, 'archive/create_archive_new.html', context)


class ArchiveListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'archive/list_archive.html'
    model = Archive
    context_object_name = 'all_archives'
    permission_required = 'archive.archive_edit'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ArchiveListView, self).get_context_data(**kwargs)
        archives = Archive.objects.all().values_list('pk', flat=True)
        context["counter_folders"] = {}
        context["counter_subfolders"] = {}
        context["counter_files"] = {}
        for arch in archives:
            template = Collection.objects.filter(parent_collection_id__isnull=True) \
                .filter(archive_id=arch).values_list('pk', flat=True).count()
            subfolders = Collection.objects.filter(parent_collection_id__isnull=False).filter(archive_id=arch) \
                .values_list('pk', flat=True)
            filecount = 0
            for folder in subfolders:
                filecount += UploadedFile.objects.filter(collection_id=folder).count()
            context["counter_folders"].update({str(arch): str(template)})
            context["counter_subfolders"].update({str(arch): str(subfolders.count())})
            context["counter_files"].update({str(arch): str(filecount)})
        return context


@login_required
def get_archive(request, pk):
    requested_archive = Archive.objects.get(id=pk)
    containing_collections = Collection.objects.filter(archive_id=pk).filter(parent_collection_id__isnull=True)
    containing_files = UploadedFile.objects.filter(collection_id=pk)
    parent_archive = Archive.objects.get(id=requested_archive.id)
    create_form = CollectionForm(parent_archive, None)
    p = Paginator(Collection.objects.filter(archive_id=pk).filter(parent_collection_id__isnull=True), 3)
    page = request.GET.get('page')
    collection = p.get_page(page)

    context = {'requested_archive': requested_archive,
               'containing_collections': containing_collections,
               'containing_files': containing_files,
               'create_form': create_form,
               'collection': collection
               }
    return render(request, 'archive/list_selected_archive.html', context)


class ArchiveUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'archive/update_archive.html'
    model = Archive
    fields = '__all__'
    success_url = reverse_lazy('list-archives')
    permission_required = 'archive.add_archive'


class ArchiveDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'archive/delete_archive.html'
    model = Archive
    context_object_name = 'archive'
    success_url = reverse_lazy('list-archives')
    permission_required = 'archive.add_archive'


class ArchivedFileCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'archived_file/create_archived_file.html'
    form_class = UploadFileForm
    # success_url = reverse_lazy('list-archives')
    permission_required = 'archived_file.add_uploadedfile'

    def __init__(self):
        super().__init__()
        self.collection_id = None

    def form_valid(self, form):
        form.instance.created_by = UserExtend.objects.get(id=self.request.user.id)
        form.instance.collection = Collection.objects.get(id=self.kwargs.get('collection_id'))

        archive_id = Collection.objects.get(id=self.kwargs.get('collection_id')).archive_id
        form.instance.document_template = DocumentTemplate.objects.get(archive_id=archive_id)
        template_file = DocumentTemplate.objects.get(archive_id=archive_id).template_file
        if template_file:
            form.instance.is_ocr = 0
        else:
            form.instance.is_ocr = 1
        self.collection_id = self.kwargs.get('collection_id')
        return super().form_valid(form)

    def get_success_url(self):
        return f'/archive/archive/list-collection/{self.collection_id}/?page=1'


class ArchivedFileListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'archived_file/list_archived_file.html'
    model = UploadedFile
    context_object_name = 'all_archived_files'
    permission_required = 'archived_file.view_uploadedfile'


@login_required
@xframe_options_sameorigin
def display_file(request, pk):
    if request.method == "POST":
        print(request.POST)
    requested_file = UploadedFile.objects.get(id=pk)
    is_ocr = UploadedFile.objects.get(id=pk).is_ocr.real
    if is_ocr == 1:
        dict_regions = UploadedFile.objects.get(id=pk).search_metadata
        regions = ast.literal_eval(dict_regions)
    else:
        dict_regions = RegionOfInterest.objects.filter(
            document_template=requested_file.document_template.id).values_list(
            'region_points', flat=True).last()
        regions = {item[3]: "" for item in ast.literal_eval(dict_regions)}

    context = {
        'requested_file': requested_file,
        'regions': regions
    }

    response = render(request, 'archived_file/list_archived_file.html', context)
    response['X-Frame-Options'] = 'SAMEORIGIN'
    return response


class ArchivedFileUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'archived_file/update_archived_file.html'
    model = UploadedFile
    fields = '__all__'
    success_url = reverse_lazy('list-archives')

    def get_context_data(self, **kwargs):
        context = super(ArchivedFileUpdateView, self).get_context_data()
        context['search_metadata'] = ast.literal_eval(self.get_object().search_metadata)
        context['filename'] = self.get_object().filename
        return context

    def get_success_url(self):
        return f'/archive/archive/list-collection/{self.object.collection_id}/?page=1'


@login_required
def delete_archived_file(request, pk):
    coll_id = None
    if request.method == 'POST':
        archived_file = UploadedFile.objects.get(pk=pk)
        archived_file.delete()
        coll_id = archived_file.collection.id
    return redirect(f'/archive/archive/list-collection/{coll_id}/?page=1')


class ArchivedFileDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'archived_file/delete_archived_file.html'
    model = UploadedFile
    context_object_name = 'file'
    success_url = reverse_lazy('list-archives')
    permission_required = 'uploadedfile.delete_uploadedfile'


class CollectionCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'archive/create_collection.html'
    model = Collection
    fields = '__all__'
    permission_required = 'archived_file.add_uploadedfile'

    def __init__(self, **kwargs):
        super().__init__()
        self.collection = None

    def form_valid(self, form: forms.ModelForm):
        self.collection = form['parent_collection'].value()
        return super().form_valid(form)

    def get_success_url(self):
        return f'/archive/archive/list-collection/{self.collection}/?page=1'


@login_required
def get_collection(request, pk):
    requested_collection = Collection.objects.get(id=pk)
    containing_collections = Collection.objects.filter(parent_collection=requested_collection).order_by(
        'collection_name').reverse()
    p1 = Paginator(UploadedFile.objects.filter(collection_id=pk), 20)
    page1 = request.GET.get('page')
    containing_files = p1.get_page(page1)
    parent_archive = Archive.objects.get(id=requested_collection.archive_id)
    create_form = CollectionForm(parent_archive, pk)
    upload_file_form = UploadFileForm()

    p = Paginator(Collection.objects.filter(parent_collection=requested_collection), 20)
    page = request.GET.get('page')
    collection = p.get_page(page)

    context = {'requested_archive': requested_collection,
               'containing_collections': containing_collections,
               'containing_files': containing_files,
               'create_form': create_form,
               'upload_file_form': upload_file_form,
               'collection': collection,
               'collection_id': pk
               }
    return render(request, 'archive/list_selected_archive.html', context)


class CollectionDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'archive/delete_collection.html'
    model = Collection
    context_object_name = 'collection'
    permission_required = 'collection.delete_collection'

    def get_success_url(self):
        return reverse_lazy('list-selected-collection', kwargs={'pk': self.object.parent_collection.id})


class CollectionUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'archive/update_collection.html'
    model = Collection
    fields = ['collection_name']
    permission_required = 'collection.change_collection'

    def get_success_url(self):
        return reverse_lazy('list-selected-collection', kwargs={'pk': self.object.parent_collection.id})


@login_required
def search_view(request):
    context = {}

    query = ""
    if request.GET:
        query = request.GET['q']
        context['query'] = str(query)
    p = Paginator(UploadedFile.objects.filter(search_metadata__icontains=query), 20)
    page = request.GET.get('page')
    collection = p.get_page(page)

    context['collection'] = collection

    return render(request, 'archived_file/search.html', context)


class DocumentTemplateCreateView(CreateView):
    template_name = 'archive/create_dt.html'
    form_class = DocumentTemplateForm
