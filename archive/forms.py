from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Layout
from django.forms import *

from archive.models import *


class ArchiveForm(ModelForm):
    class Meta:
        model = Archive
        fields = '__all__'
        widgets = {
            'archive_name': TextInput(attrs={
                'placeholder': 'Please insert Archive name',
                'class': 'form-control'
            }),
            'description': TextInput(attrs={
                'placeholder': 'Please insert Archive description',
                'class': 'form-control'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(ArchiveForm, self).__init__(*args, **kwargs)
        self.fields['archive_name'].required = True
        self.fields['description'].required = False
        self.fields['created_by'].widget = HiddenInput()

    def clean(self):
        cleaned_data = self.cleaned_data
        get_name = cleaned_data.get('archive_name')
        all_archives = Archive.objects.all()
        for archive in all_archives:
            if archive.archive_name == get_name:
                msg = 'This Archive already exist'
                self._errors['archive_name'] = self.error_class([msg])
        return cleaned_data


class CollectionForm(ModelForm):
    class Meta:
        model = Collection
        fields = '__all__'
        widgets = {
            'collection_name': TextInput(attrs={
                'placeholder': 'Please insert Collection name',
                'class': 'form-control'
            })
        }

    def __init__(self, archive=None, parent_collection=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['archive'].widget = HiddenInput()
        self.fields['parent_collection'].widget = HiddenInput()
        if archive:
            self.fields['archive'].initial = archive
        if parent_collection:
            self.fields['parent_collection'].initial = parent_collection


class DocumentTemplateForm(ModelForm):
    class Meta:
        model = DocumentTemplate
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DocumentTemplateForm, self).__init__(*args, **kwargs)
        self.fields['dt_name'].widget.attrs['id'] = 'dt_name'
        self.fields['template_file'].widget.attrs['id'] = 'template_file'
        self.fields['archive'].widget = HiddenInput()


class RegionOfInterestForm(ModelForm):
    class Meta:
        model = RegionOfInterest
        fields = '__all__'
        labels = {
            'roi_name': 'Region name',
            "region_points": u"Region Points"
        }
        widgets = {
            'roi_name': TextInput(attrs={
                'placeholder': 'Please insert the name of the field',
                'class': 'form-control'
            }),
            'region_points': TextInput(attrs={
                'id': 'region_points'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(RegionOfInterestForm, self).__init__(*args, **kwargs)
        self.fields['roi_name'].required = True
        self.fields['region_points'].widget = HiddenInput()
        self.fields['document_template'].widget = HiddenInput()


class UploadFileForm(ModelForm):
    class Meta:
        model = UploadedFile
        fields = '__all__'

        labels = {
            'filename': 'Select File to Upload'
        }

    def __init__(self, created_by=None, collection=None, *args, **kwargs):
        super(UploadFileForm, self).__init__(*args, **kwargs)
        self.fields['filename'].required = True
        self.fields['search_metadata'].widget = HiddenInput()
        self.fields['is_ocr'].widget = HiddenInput()
        self.fields['document_template'].widget = HiddenInput()
        self.fields['created_by'].widget = HiddenInput()
        self.fields['collection'].widget = HiddenInput()
        if created_by:
            self.fields['created_by'].initial = created_by
        if collection:
            self.fields['collection'].initial = collection



