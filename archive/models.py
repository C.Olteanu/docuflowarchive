import ast
import cv2
import numpy as np
import pytesseract
import os
import threading
import fitz

from typing import Tuple
from django.db import models

from user.models import UserExtend

NAME_LENGTH = 25


class Archive(models.Model):
    archive_name = models.CharField(max_length=NAME_LENGTH, null=False, blank=False)
    description = models.CharField(max_length=50, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(UserExtend, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.archive_name


class DocumentTemplate(models.Model):
    dt_name = models.CharField(max_length=NAME_LENGTH, null=False, blank=False)
    template_file = models.FileField(null=True, blank=True, upload_to='DocumentTemplate/')
    archive = models.ForeignKey(Archive, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.dt_name

    def delete(self, *args, **kwargs):
        self.template_file.delete(save=False)
        super().delete(*args, **kwargs)


class RegionOfInterest(models.Model):
    roi_name = models.CharField(max_length=NAME_LENGTH, null=False, blank=False)
    region_points = models.TextField(null=True, blank=True)
    document_template = models.ForeignKey(DocumentTemplate, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.roi_name


class Collection(models.Model):
    collection_name = models.CharField(max_length=NAME_LENGTH, null=False, blank=False)
    parent_collection = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    archive = models.ForeignKey(Archive, on_delete=models.CASCADE)

    def __str__(self):
        return self.collection_name


class UploadedFile(models.Model):
    document_template = models.ForeignKey(DocumentTemplate, on_delete=models.SET_NULL, null=True, blank=True)
    filename = models.FileField(null=False, blank=False, upload_to='pdfs/')
    search_metadata = models.CharField(max_length=254, null=True, blank=True)
    is_ocr = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(UserExtend, on_delete=models.DO_NOTHING, null=True, blank=True)
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.filename}'

    def file_name(self):
        base = self.filename.name
        return os.path.splitext(base)[0]

    def delete(self, *args, **kwargs):
        self.filename.delete(save=False)
        super(UploadedFile, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.pk is not None:
            super(UploadedFile, self).save(*args, **kwargs)
        else:
            super(UploadedFile, self).save(*args, **kwargs)
            region_points = RegionOfInterest.objects.get(document_template_id=self.document_template.id).region_points
            threading.Thread(target=do_ocr,
                             args=(str(self.document_template.template_file.file), ast.literal_eval(region_points),
                                   str(self.filename.file),
                                   self.id)).start()


def do_ocr(template_file, roi, file_to_ocr, file_id):
    per = 100
    extension = os.path.splitext(file_to_ocr)[1][1:]
    converted = 0
    if str.lower(extension) == 'pdf':
        file_to_ocr = convert_pdf2img(file_to_ocr)
        file_to_delete = file_to_ocr
        converted += 1
    pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
    imgOriginal = cv2.imread(template_file)
    # h, w, c = imgOriginal.shape
    orb = cv2.ORB_create(1000)  # creeaza punctele comune
    kp1, desc1 = orb.detectAndCompute(imgOriginal, None)  # punctele sunt creeate pe imaginea initiala
    img = cv2.imread(file_to_ocr)
    kp2, desc2 = orb.detectAndCompute(img, None)  # punctele sunt creeate pe imaginea care urmeaza a fi folosita
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    matches = bf.match(desc2, desc1)  # coreleaza punctele de pe cele doua imagini
    matches.sort(key=lambda xd: xd.distance)
    good = matches[:int(len(matches) * (per / 100))]  # selectam un procentaj de puncte comune bune
    srcPoint = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    destPoint = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    M, _ = cv2.findHomography(srcPoint, destPoint, cv2.RANSAC, 5.0)
    # imgScan = cv2.warpPerspective(img, M, (w, h))  # pozitioneaza imaginea cat mai aproape de original
    imgShow = img.copy()

    myData = {}
    for x, r in enumerate(roi):
        imgCrop = imgShow[r[0][1]:r[1][1], r[0][0]:r[1][0]]  # face crop din dreptunghiul de mai sus
        if r[2] == 'text':  # daca tipul campului este text, crop-ul va fi trimis spre ocr
            myData[r[3]] = (pytesseract.image_to_string(imgCrop)).replace("\n", " ")
    if converted == 1:
        if os.path.exists(file_to_delete):
            os.remove(file_to_delete)
    UploadedFile.objects.filter(id=file_id).update(search_metadata=myData, is_ocr=1)


def convert_pdf2img(input_file: str, pages: Tuple = None):
    """Converts pdf to image and generates a file by page"""
    # Open the document
    pdfIn = fitz.open(input_file)
    output_files = []
    # Iterate throughout the pages
    for pg in range(pdfIn.pageCount):
        if str(pages) != str(None):
            if str(pg) not in str(pages):
                continue
        if pg == 0:
            # Select a page
            page = pdfIn[pg]
            rotate = int(0)
            # PDF Page is converted into a whole picture 1056*816 and then for each picture a screenshot is taken.
            # zoom = 1.33333333 -----> Image size = 1056*816 zoom = 2 ---> 2 * Default Resolution (text is clear,
            # image text is hard to read)    = filesize small / Image size = 1584*1224 zoom = 4 ---> 4 * Default
            # Resolution (text is clear, image text is barely readable) = filesize large zoom = 8 ---> 8 * Default
            # Resolution (text is clear, image text is readable) = filesize large
            zoom_x = 2.9
            zoom_y = 2.9
            # The zoom factor is equal to 2 in order to make text clear
            # Pre-rotate is to rotate if needed.
            mat = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
            pix = page.getPixmap(matrix=mat, alpha=False)
            output_file = f"{os.path.splitext(os.path.basename(input_file))[0]}_page{pg + 1}.png"
            pix.writePNG(output_file)
            output_files.append(output_file)
    pdfIn.close()
    return output_files[0]
