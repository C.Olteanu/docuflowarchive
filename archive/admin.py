from django.contrib import admin

from archive.models import *

admin.site.register(Archive)
admin.site.register(UploadedFile)
admin.site.register(Collection)
admin.site.register(DocumentTemplate)
admin.site.register(RegionOfInterest)

