from django.urls import path

from archive import views

urlpatterns = [
    path('archive/create-archive/', views.ArchiveCreateView.as_view(), name='create-archive'),
    path('archive/list-archives/', views.ArchiveListView.as_view(), name='list-archives'),
    path('archive/list-archives/<int:pk>/', views.get_archive, name='list-selected-archive'),
    path('archive/update-archive/', views.ArchiveUpdateView.as_view(), name='update-archive'),
    path('archive/delete-archive/<int:pk>/', views.ArchiveDeleteView.as_view(), name='delete-archive'),
    path('archive/create-collection/', views.CollectionCreateView.as_view(), name='create-collection'),
    path('archive/list-collection/<int:pk>/', views.get_collection, name='list-selected-collection'),
    path('archive/view-file/<int:pk>/', views.display_file, name='list-archived-file'),
    path('archive/delete-collection/<int:pk>/', views.CollectionDeleteView.as_view(), name='delete-collection'),
    path('archive/update-collection/<int:pk>/', views.CollectionUpdateView.as_view(), name='update-collection'),
    path('archive/create-parrent-collection/', views.archive_create_view, name='create-new-collection'),
    path('archive/search/', views.search_view, name='search'),
    path('file/upload/<int:collection_id>/', views.ArchivedFileCreateView.as_view(), name='file-upload'),
    path('file/update/<int:pk>/', views.ArchivedFileUpdateView.as_view(), name='file-update'),
    path('file/<int:pk>/', views.ArchivedFileDeleteView.as_view(), name='file-delete'),
]
