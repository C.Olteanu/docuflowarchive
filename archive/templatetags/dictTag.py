from django.template.defaulttags import register


@register.filter
def get_item(dictionary, key):
    if dictionary.get(str(key)) is None:
        return 0
    else:
        return dictionary.get(str(key))


@register.filter
def index(anylist, i):
    return anylist[i]
