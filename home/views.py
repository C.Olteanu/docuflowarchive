from django.http import FileResponse, Http404
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.decorators.clickjacking import xframe_options_sameorigin


@xframe_options_sameorigin
def homepage_view(request):
    if request.user.is_anonymous:
        return redirect(reverse_lazy('login'))
    else:
        return render(request, 'home/home.html', {})


@xframe_options_sameorigin
def pdf(request):
    try:
        return FileResponse(open('media/pdfs/test_factura.pdf', 'rb'), content_type='application/pdf')
    except FileNotFoundError:
        raise Http404('not found')
