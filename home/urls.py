from django.conf.urls import url
from django.urls import path

from home import views

urlpatterns = [

    path('', views.homepage_view, name='homePage'),
    url(r'^pdf', views.pdf, name='pdf'),

]
